from game_objects.vector import Vector
from game_objects.game_object import GameObject


class Ball(GameObject):
    def __init__(self, speed, size, y):
        super().__init__(size, 199, y)
        self.speed = speed
