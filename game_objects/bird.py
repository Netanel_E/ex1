from game_objects.vector import Vector
from game_objects.game_object import GameObject


class Bird(GameObject):
    def __init__(self, size, x, y):
        super().__init__(size, x, y)

    def keyboard_move(self, x, y):
        "Move bird up in response to keyboard arrows tap."
        key_move = Vector(x, y)
        self.vector.move(key_move)

    def tap(self, x, y):
        "Move bird up in response to screen tap."
        up = Vector(0, 30)
        self.vector.move(up)
