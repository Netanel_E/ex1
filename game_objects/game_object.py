from game_objects.vector import Vector
from abc import ABC


class GameObject(ABC):
    def __init__(self, size, x, y):
        if size < 0:
            raise ValueError
        self.size = size
        self.vector = Vector(x, y)
