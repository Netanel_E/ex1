class Vector:
    """
    Class for two-dimensional vectors with methods: move, add, sub and abs
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self, vector):
        """
        move Vector by other vector (add other vector to itself)
        :param vector: vector to add to another vector

        >> v = Vector(1, 2)
        >> w = Vector(3, 4)
        >> v.move(w)
        >> v
        vector(4, 6)
        """
        if isinstance(vector, Vector):
            self.x += vector.x
            self.y += vector.y
        else:
            self.x += vector
            self.y += vector

    def __add__(self, vector):
        """
        add operation for vectors - add two vectors and return new vector
        :param vector: vector to add with other vector
        :return: new vector which is the addition of the two

        >> v = Vector(1, 2)
        >> w = Vector(3, 4)
        >> v + w
        vector(4, 6)
        >> v
        Vector(1, 2)
        """
        if isinstance(vector, Vector):
            return Vector(self.x + vector.x, self.y + vector.y)
        else:
            return Vector(self.x + vector, self.y + vector)

    def __sub__(self, vector):
        """
        sub operation for vectors - sub two vectors and return new vector
        :param vector: vector to sub with other vector
        :return: new vector which is the subtraction of the two

        >> v = Vector(1, 2)
        >> w = Vector(3, 4)
        >> v - w
        vector(-2, -2)
        >> v
        Vector(1, 2)
        """
        if isinstance(vector, Vector):
            return Vector(self.x - vector.x, self.y - vector.y)
        else:
            return Vector(self.x - vector, self.y - vector)

    def __abs__(self):
        """
        abs operation for vectors
        >> v = vector(3, 4)
        >> abs(v)
        5.0
        """
        return (self.x ** 2 + self.y ** 2) ** 0.5
