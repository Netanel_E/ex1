"""Flappy, game inspired by Flappy Bird.

Exercises

1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.

"""
from turtle import setup, hideturtle, up, tracer, onscreenclick, listen, onkey, done
from game_objects import Bird
from games import Game


if __name__ == '__main__':
    bird = Bird(10, 0, 0)
    game = Game(bird)
    setup(420, 420, 370, 0)
    hideturtle()
    up()
    tracer(False)
    onscreenclick(bird.tap)
    game.writer.goto(-30, 180)
    game.writer.color('black')
    game.writer.write(game.score)
    # listen to keyboard
    listen()
    onkey(lambda: bird.keyboard_move(0, 30), "Up")
    onkey(lambda: bird.keyboard_move(-30, 0), "Left")
    onkey(lambda: bird.keyboard_move(30, 0), "Right")

    game.move()
    done()
