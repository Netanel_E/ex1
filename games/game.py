from turtle import ontimer, clear, goto, Turtle, dot
from random import randint, randrange
from game_objects.ball import Ball


class Game:
    def __init__(self, bird):
        self.score = 0
        self.bird = bird
        self.writer = Turtle(visible=False)
        self.balls = []

    def inside(self, point):
        "Return True if point on screen."
        return -200 < point.x < 200 and -200 < point.y < 200

    def draw(self, alive):
        "Draw screen objects."
        clear()
        goto(self.bird.vector.x, self.bird.vector.y)

        if alive:
            self.score += 1  # get points while still alive
            dot(self.bird.size, 'green')
        else:
            for ball in self.balls:  # when game is over, hide all balls
                ball.size = 0
            dot(self.bird.size, 'red')

        for ball in self.balls:
            goto(ball.vector.x, ball.vector.y)
            dot(ball.size, 'black')  # each ball get his own size
        # update()

    def move(self):
        "Update object positions."
        # write high score to screen
        self.writer.undo()
        self.writer.write("Score: {}".format(self.score), False, align="left", font={"Arial", 8, "normal"})

        self.bird.vector.y -= 5  # move down the bird every second - like 'gravity'

        for ball in self.balls:
            ball.vector.x -= ball.speed  # each ball moves with his own speed

        if randrange(10) == 0:
            y = randint(-199, 199)
            size = randint(10, 50)
            speed = randint(1, 10)
            ball = Ball(speed, size, y)  # instantiate each ball with his own attributes
            self.balls.append(ball)

        while len(self.balls) > 0 and not self.inside(self.balls[0].vector):
            self.balls.pop(0)

        if not self.inside(self.bird.vector):
            self.draw(False)
            return

        for ball in self.balls:
            if abs(ball.vector - self.bird.vector) < 5 + ball.size / 2:
                self.draw(False)
                return

        self.draw(True)
        ontimer(self.move, 50)
