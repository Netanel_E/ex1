import pytest
from game_objects import vector
from game_objects import ball


def test_create_vector():
    v = vector.Vector(2, 3)
    assert v.x == 2
    assert v.y == 3


def test_vector_move():
    v = vector.Vector(2, 3)
    w = vector.Vector(2, 3)
    v.move(w)
    assert v.x == 4
    assert v.y == 6


def test_vector_add():
    v = vector.Vector(2, 3)
    w = vector.Vector(2, 3)
    t = v + w
    assert t.x == 4
    assert t.y == 6

    s = v + 3
    assert s.x == 5
    assert s.y == 6


def test_vector_sub():
    v = vector.Vector(2, 3)
    w = vector.Vector(2, -3)
    t = v - w
    assert t.x == 0
    assert t.y == 6

    s = v - 3
    assert s.x == -1
    assert s.y == 0


def test_vector_abs():
    v = vector.Vector(3, 4)
    assert abs(v) == 5


def test_create_ball():
    a_ball = ball.Ball(10, 50, 100)
    assert a_ball.speed == 10
    assert a_ball.size == 50
    assert a_ball.vector.x == 199
    assert a_ball.vector.y == 100


def test_move_ball():
    a_ball = ball.Ball(10, 50, 100)
    v = vector.Vector(-9, 10)
    a_ball.vector.move(v)
    assert a_ball.speed == 10
    assert a_ball.size == 50
    assert a_ball.vector.x == 190
    assert a_ball.vector.y == 110
